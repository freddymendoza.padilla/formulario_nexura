$(document).ready(function() {
	consultar_info()
	consultar_data()
	 $("#area").select2({
             placeholder: "Seleccione un area",
             allowClear: true    
    });
	
	 $("#area_edit").select2({
             placeholder: "Seleccione un area",
             allowClear: true    
    });
	
	 $("#frm").validate({
	 	
		    rules: {
		      nombres : {
		        required: true,
		        minlength: 3
		      },
		      sexo: {
		        required: true
		      
		      },
		      email: {
		        required: true,
		        email: true
		      }, 
		      area: {
		        required: true
		      },
		      descripcion: {
		        required: true
		      },
		       rol: {
		        required: true
		      }
		    },
		    messages: {
		        nombres: "El campo Nombre es obligatorio (de 2 a 50 caracteres)",
		        email: "El campo Email es obligatorio (de 4 a 100 caracteres con formato email)",
		        sexo: "El campo Sexo es obligatorio. Seleccione un campo.",
		        area: "El campo Area es obligatorio (Seleccione un area)",
		        descripcion: "El campo Descripcion es obligatorio (de 2 a 200 caracteres)",
		        rol:"El campo Roles es obligatorio, puede seleccionar multiples roles "
		    },
    errorElement : 'span'
    });

});

/*consulta los checbox y el select*/
function consultar_info(){
	$.ajax({
		url: 'controllers/formulario.php',
		type: 'POST',
		dataType: 'json',
		data: {opcn: 'consultar_info'},
	})
	.done(function(data) {
		console.log(data);
		let area="";
		let roles="";
		let roles_edit = "";
		// let contador = 0;
			for (var i = 0; i < data.areas.length; i++) {
                area += '<option value="' + data.areas[i].area_id + '">' + data.areas[i].nombre +' </option>';  		
            }

            for (var i = 0; i < data.roles.length; i++) {
            
                roles +='<input type="checkbox" class="check" id="checkbox'+data.roles[i].id+'"  name="rol" value="'+data.roles[i].id+'"> <label> ' + data.roles[i].nombre +' </label><br>'
                roles_edit +='<input type="checkbox" class="check_edit" id="checkbox_edit'+data.roles[i].id+'"  name="rol" value="'+data.roles[i].id+'"> <label> ' + data.roles[i].nombre +' </label><br>'
            }
            

             $('#area').append(area);
             $('#checkboxinfo').append(roles);
             $('#area_edit').html(area);
             $('#checkboxinfo_edit').append(roles_edit);
             
	})
	.fail(function() {
		console.log("error");
	})
	
}
/* Insertar Empleado */
$('#frm').submit(function(event) {
	/* Act on the event */
	 event.preventDefault()
	 let rol = [];

	 info = $(this).serializeArray();
	$('.check').each(function(index, el) {
			rol.push({name:$(this).attr('value') , value: $(this).prop('checked') })
		  
		}); 
	 
	$.ajax({
		url: 'controllers/formulario.php',
		type: 'POST',
		dataType: 'JSON',
		data: {info,rol, opcn: 'insertar'}
	})
	.done(function(data) {
		
		Swal.fire({
			  position: 'top-end',
			  icon: 'success',
			  title: data.msj,
			  showConfirmButton: false,
			  timer: 3000
			})
		$("#frm")[0].reset();
	    $("#addUserModal").modal("hide")
		consultar_data()
		
	})
	.fail(function() {
		console.log("error");
	})
});

/*consulta los empleados para pintar en la tabla*/
function consultar_data(){
	$.ajax({
		url: 'controllers/formulario.php',
		type: 'POST',
		dataType: 'json',
		data: {opcn: 'consultar_data'},
	})
	.done(function(data) {
		 console.log(data);
		let table;
         let btn_elimina;
		for (var i = 0; i < data.length; i++) {
			btn_eliminar = '<a  id="eliminar" data-id_empleado="'+data[i].id+'" class="btn btn-danger pull-right" ><i class="fa fa-pencil-square" aria-hidden="true"></i><span><i class="fa fa-trash" aria-hidden="true"></i></span></a>'
			btn_editar = '<a href="#editUserModal" data-toggle="modal" id="editar" data-id_empleado="'+data[i].id+'" class="btn btn-success pull-right" > <span><i class="fa fa-edit" aria-hidden="true"></i> </span></a>'
			
			  table += '<tr>'
		      table += '<td><p class="text-black">' + data[i].nombre + '</p></td>'
		      table += '<td><p class="text-black">' + data[i].email + '</p></td>'
		      if (data[i].sexo == "M") {
		      	table += '<td>Masculino</td>'
		      }else{
		      	table += '<td>Femenino</td>'
		      }
		      table += '<td><p class="text-black">' + data[i].area + '</p></td>'
		      if (data[i].boletin == 1) {
		      		table += '<td><p class="text-black">Si</p></td>'
		      }else{
					table += '<td><p class="text-black">No</p></td>'
		      }
		    
		      	table += '<td><p class="text-black">' + data[i].roles + '</p></td>'
		     
		      
		      table += '<td><p class="text-black">' + btn_editar + '</p></td>'
		      table += '<td><p class="text-black">' + btn_eliminar + '</p></td>'
		      table += '</tr>'
		}
		$('#tabla_usuarios tbody').html(table)
	})
	.fail(function() {
		console.log("error");
	})
	
}

/*eliminar los usuarios*/
$('body').on('click', '#eliminar', function(event) {
	event.preventDefault();
	let id = $(this).data('id_empleado')
	swal({
  title: "Desea eliminar este usuario?",
  //text: "Once deleted, you will not be able to recover this imaginary file!",
  icon: "warning",
  buttons: true,
  confirmButtonText: 'SI',
  denyButtonText: `No deseo eliminar`,
  dangerMode: true,
})
.then((willDelete) => {
  if (willDelete) {
  	$.ajax({
		url: 'controllers/formulario.php',
		type: 'POST',
		dataType: 'JSON',
		data: {opcn: 'eliminar', id},
	})
	.done(function(data) {
		console.log(data);
		swal("Eliminado!", {
        icon: "success",

    });
		consultar_data()
	})
	.fail(function() {
		console.log("error");
	})
    
  } else {
    swal("No se realizaron cambios");
  }
 
});

	
	
});

/*eliminar los usuarios*/
$('body').on('click', '#editar', function(event) {
	event.preventDefault();
	let id = $(this).data('id_empleado')
	$('#id').val(id);
	$.ajax({
		url: 'controllers/formulario.php',
		type: 'POST',
		dataType: 'JSON',
		data: {opcn: 'consultar_empleado', id},
	})
	.done(function(data) {
		console.log(data);
		
		for (var i = 0; i < data.empleado.length; i++) {
			//console.log(data.empleado[i])
			$('#nombres_edit').val(data.empleado[i].nombre)
			$('#email_edit').val(data.empleado[i].email)
			if (data.empleado[i].sexo === "M") {
				$("#opc_1_edit").prop("checked", true);
			}else{
				$("#opc_2_edit").prop("checked", true);
			}
			$("#area_edit").val(data.empleado[i].area_id).trigger('change');
			$('#descripcion_edit').val(data.empleado[i].descripcion)

			if (data.empleado[i].boletin == 1) {
				$('#boletin_edit').prop("checked", true);
			}else{
				$('#boletin_edit').prop("checked", false);
			}


		}
			
		for (var i = 0; i < data.rol.length; i++) {
				// console.log(data.rol[i].rol_id)
				if (data.rol[i].rol_id == 1) {
					$('#checkbox_edit1').prop("checked", true);
				} else if(data.rol[i].rol_id == 2){
					$('#checkbox_edit2').prop("checked", true);
				}else if(data.rol[i].rol_id == 3){
					$('#checkbox_edit3').prop("checked", true);
				}
			}	
		
		
	})
	.fail(function() {
		console.log("error");
	})
	
});

/* Editar empleado */

$('#frm_edit').submit(function(event) {
	/* Act on the event */
	 event.preventDefault()
	 let rol = [];

	 info = $(this).serializeArray();
	$('.check_edit').each(function(index, el) {
			rol.push({name:$(this).attr('value') , value: $(this).prop('checked') })
		  
		}); 
	 
	$.ajax({
		url: 'controllers/formulario.php',
		type: 'POST',
		dataType: 'JSON',
		data: {info, rol,opcn: 'editar'}
	})
	.done(function(data) {
		
		Swal.fire({
			  position: 'top-end',
			  icon: 'success',
			  title: data.msj,
			  showConfirmButton: false,
			  timer: 3000
			})
		$("#frm")[0].reset();
	    $("#editUserModal").modal("hide")
		consultar_data()
		
	})
	.fail(function() {
		console.log("error");
	})
});