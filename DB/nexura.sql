-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 14-09-2021 a las 17:52:39
-- Versión del servidor: 10.4.14-MariaDB
-- Versión de PHP: 7.2.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `nexura`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `areas`
--

CREATE TABLE `areas` (
  `area_id` int(11) NOT NULL COMMENT 'Identificador del área',
  `nombre` varchar(255) NOT NULL COMMENT 'Nombre del área de la empresa'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `areas`
--

INSERT INTO `areas` (`area_id`, `nombre`) VALUES
(1, 'Ventas'),
(2, 'Calidad'),
(3, 'Produccion');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empleados`
--

CREATE TABLE `empleados` (
  `id` int(11) NOT NULL COMMENT 'Identificador del empleado',
  `nombre` varchar(255) NOT NULL COMMENT 'Nombre completo del empleado como tipo text, solo debe permitir letras con o sin tildes y espacios, no se adminten caracteres especiales ni numeros',
  `email` varchar(255) NOT NULL COMMENT 'Correo electrónico del empleado, campo de tipo text | email, solo se permite una estructura de correo, obligatorio',
  `sexo` char(1) NOT NULL COMMENT 'Campo de tipo radio button, M para masculino f para femenino, Obligatorio',
  `area_id` int(11) NOT NULL COMMENT 'area de la empresa a la que pertenece el empleado, campo de tipo Select, obloigatorio',
  `boletin` int(11) NOT NULL COMMENT '1 para recibir boletín, 0 para no recibir boletín, campo de tipo check box. opcional',
  `descripcion` text NOT NULL COMMENT 'Se describe la experienca del empleado, campo de tipo text area abligatorio',
  `estado_id` int(2) NOT NULL DEFAULT 1 COMMENT '1 habilitado, 2: inhabilitado.\r\nse maneja un estado para llevar la trazabilidad del usuario'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `empleados`
--

INSERT INTO `empleados` (`id`, `nombre`, `email`, `sexo`, `area_id`, `boletin`, `descripcion`, `estado_id`) VALUES
(2, 'ana rangel martinez', 'freddymendoza.padilla1@gmail.com', 'M', 1, 1, 'fred', 2),
(3, 'ana rangel martinez', 'freddymendoza.padilla1@gmail.com', 'M', 1, 1, 'fred', 2),
(4, 'junior mendoza', 'freddymendoza.padilla1@gmail.com', 'M', 1, 1, 'descr', 1),
(5, 'freddy mendoza padilla', 'freddymendoza.padilla1@gmail.com', 'M', 1, 1, 'descr', 1),
(6, 'ana rangel martinez', 'freddymendoza.padilla1@gmail.com', '', 1, 0, '', 2),
(7, 'indira', 'freddymendoza.padilla1@gmail.com', 'F', 1, 1, 'vane', 1),
(8, 'freed', 'freddymendoza.padilla1@gmail.com', 'F', 2, 1, 'des', 2),
(9, 'laura', 'laura@gmail.com', 'F', 2, 0, 'esto es una prueba', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empleado_rol`
--

CREATE TABLE `empleado_rol` (
  `empleado_id` int(11) NOT NULL COMMENT 'Identificador del empleado',
  `rol_id` int(11) NOT NULL COMMENT 'Identificador del rol'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `empleado_rol`
--

INSERT INTO `empleado_rol` (`empleado_id`, `rol_id`) VALUES
(5, 1),
(5, 2),
(5, 3),
(6, 1),
(6, 2),
(6, 3),
(8, 1),
(8, 2),
(8, 3),
(7, 1),
(9, 1),
(9, 2),
(9, 3),
(4, 1),
(4, 2),
(4, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles`
--

CREATE TABLE `roles` (
  `id` int(11) NOT NULL COMMENT 'identificador del rol',
  `nombre` varchar(255) NOT NULL COMMENT 'Nombre del rol'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `roles`
--

INSERT INTO `roles` (`id`, `nombre`) VALUES
(1, 'Profecional de proyectos - Desarrollador'),
(2, 'Gerente estratégico'),
(3, 'Auxiliar administrativo');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `areas`
--
ALTER TABLE `areas`
  ADD PRIMARY KEY (`area_id`);

--
-- Indices de la tabla `empleados`
--
ALTER TABLE `empleados`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `areas`
--
ALTER TABLE `areas`
  MODIFY `area_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Identificador del área', AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `empleados`
--
ALTER TABLE `empleados`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Identificador del empleado', AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'identificador del rol', AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
