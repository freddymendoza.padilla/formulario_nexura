<?php 
include_once('../config/init_db.php');
class Formulario{

	public static function consultar_info(){
		$info=[];
		 $info['areas']=DB::query("SELECT * FROM areas");
		 $info['roles']=DB::query("SELECT * FROM roles");

		 return $info;
		 DB::disconnect();
	}

	public static  function insertar_info($p,$data){
		extract($data);
		
		if (!isset($boletin)) {
			$boletin = 0;
		}
		
	    $query = DB::query("INSERT INTO empleados(
												   
		  										    nombre,
		  										    email,
		  										    sexo,
		  										    area_id,
		  										    boletin,
		  										    descripcion
		  										)
		  										VALUES(
		  										'$nombres',
		  										'$email',
		  										'$sexo',
		  										 $area,
		  										 $boletin,
		  										'$descripcion'
		  										)");

		  $empleado_id = DB::insertId();

		

			foreach ($p['rol'] as $key => $value) {
	         	DB::query("INSERT INTO empleado_rol
					 			                            (empleado_id, 
					 			                            rol_id)
					 			                            VALUES
					 			                            (
					 			                            $empleado_id,
					 			                            '{$p['rol'][$key]['name']}'
					 			                            )") ;
					 			                            
			}

			    	
		if ($query) {
			$result['error']=false;
			$result['msj']='Empleado creado correctamente';
		}else{
			$result['error']=true;
			$result['msj']='error al guardar';
		}
		return $result;
		DB::disconnect();
	}

	public static  function consultar_data(){
	
		$empleados = DB::query("SELECT e.*, a.nombre AS area FROM empleados e
								INNER JOIN areas a
											ON a.area_id = e.area_id
											WHERE estado_id = 1");
		$roles = DB::query("SELECT r.rol_id, r.empleado_id,GROUP_CONCAT( rl.nombre SEPARATOR ' ')  rol , e.nombre 
							 FROM empleado_rol r
								INNER JOIN empleados e
									ON e.id = r.empleado_id
										INNER JOIN roles rl
											ON rl.id = r.rol_id
                                            GROUP BY empleado_id");
		$contador=0;
		$asignacion = array();
		foreach ($empleados as $key => $value) {
				
			foreach ($roles as $key2 => $value2) {
				
				if ($empleados[$key]['id'] === $roles[$key2]['empleado_id'] ) {
					$empleados[$key]['roles'] =  $roles[$key2]['rol'];
					
				}
			}
			
		}
		//$result = array_values($empleados);
		return $empleados;
		
        DB::disconnect();
	}

	public static function eliminar_empleado($p){
		extract($p);
		$resul = DB::query("UPDATE empleados SET estado_id = 2 WHERE id = $id");

		if ($resul) {
			$result['error']=false;
			$result['msj']='Empleado eliminado correctamente';
		}else{
			$result['error']=true;
			$result['msj']='error al eliminar';
		}
		return $result;
		DB::disconnect();
	}

    public static function consultar_empleado($p){
		extract($p);
		$resul['empleado'] = DB::query("SELECT e.*, a.nombre AS area FROM empleados e
								INNER JOIN areas a
											ON a.area_id = e.area_id
											WHERE e.id = $id");

		$resul['rol'] = DB::query("SELECT r.rol_id, rl.nombre  rol 
							 FROM empleado_rol r
								INNER JOIN empleados e
									ON e.id = r.empleado_id
										INNER JOIN roles rl
											ON rl.id = r.rol_id
                                            WHERE r.empleado_id = $id");

		return $resul;
		
		DB::disconnect();
	}

	public static function editar_empleado($p,$data){
	extract($data);
		
		if (!isset($boletin_edit)) {
			$boletin_edit = 0;
		}
	
	        $query = DB::query("UPDATE
								    empleados
								SET
								    nombre = '$nombres_edit',
								    email = '$email_edit',
								    sexo = '$sexo_edit',
								    area_id = $area_edit,
								    boletin = $boletin_edit,
								    descripcion = '$descripcion_edit'
								WHERE
								    id = $id");
	   

		    DB::query("DELETE FROM empleado_rol WHERE empleado_id = $id ");
			
			foreach ($p['rol'] as $key => $value) {
				if( $p['rol'][$key]['value'] == 'false' ){continue;}
				
					   	DB::query ("INSERT INTO empleado_rol
					 			                            (empleado_id, 
					 			                            rol_id)
					 			                            VALUES
					 			                            (
					 			                            $id,
					 			                            '{$p['rol'][$key]['name']}'
					 			                            )") ;
					 			                            
			}

			    	
		if ($query) {
			$result['error']=false;
			$result['msj']='Empleado editado correctamente';
		}else{
			$result['error']=true;
			$result['msj']='error al editar';
		}
		return $result;
		DB::disconnect();
	}
}