<?php  

$method = $_SERVER['REQUEST_METHOD'];

switch ($method) {
	case 'POST':
		// code...
	include_once('../models/formulario.php');
		switch ($_POST['opcn']) {
			case 'consultar_info':
				$result = formulario::consultar_info();
				echo json_encode($result);
				break;
			case 'insertar':
			$data = Array();
			
			foreach ($_POST['info'] as $key => $value) {
				$data[$value['name']]= $value['value'];
			}
				$result = formulario::insertar_info($_POST, $data);
				echo json_encode($result);
				break;
			case 'consultar_data':
				$result = formulario::consultar_data();
				echo json_encode($result);
				break;
			case 'eliminar':
				$result = formulario::eliminar_empleado($_POST);
				echo json_encode($result);
				break;

			case 'consultar_empleado':
				$result = formulario::consultar_empleado($_POST);
				echo json_encode($result);
				break;
			case 'editar':
			$data = Array();
			
			foreach ($_POST['info'] as $key => $value) {
				$data[$value['name']]= $value['value'];
			}
				$result = formulario::editar_empleado($_POST, $data);
				echo json_encode($result);
				break;
		}
		break;	
	
}