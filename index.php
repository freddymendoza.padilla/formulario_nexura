<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Nexura</title>
	<link rel="stylesheet" href="css/mycss.css">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">
	<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/css/select2.min.css" rel="stylesheet"/>

  <link href="css/fontawesome-free/css/fontawesome.css" rel="stylesheet">
  <link href="css/fontawesome-free/css/brands.css" rel="stylesheet">
  <link href="css/fontawesome-free/css/solid.css" rel="stylesheet">

</head>
<style type="text/css" media="screen">
	textarea{
		width: 466px;
    	height: 90px;
	}
	.center{
		text-align: center;
	}
	.th{
		width:8%;
	}
</style>
<body>
<div class="container">
		<div class="table-wrapper">
			<div class="table-title">
				<div class="row">
					<div class="col-md-6">
						<h2>Lista de Empleados</h2>
					</div>
					<div class="col-md-6">
						<a href="#addUserModal" id="editar" class="btn btn-primary pull-right" data-toggle="modal"><i class="material-icons"></i> <span>Crear</span></a>

					</div>
				</div>
			</div>
			<div class="row">
					<div class='col-md-12'>
						<table id="tabla_usuarios" class="table table-striped">
							<thead>
								<tr>
									<th><i class="fa fa-user"></i></i> Nombre</th>
									<th><i class="fa fa-at" aria-hidden="true"></i> Email</th>
									<th><i class="fa fa-venus-mars" aria-hidden="true"></i> Sexo</th>
									<th class="th"><i class="fa fa-briefcase" aria-hidden="true"></i> Área</th>
									<th class="th"><i class="fa fa-envelope" aria-hidden="true"></i> Boletín</th>
									<th><i class="fa fa-address-card" aria-hidden="true"></i> Roles</th>
									<th>Modificar</th>
									<th>Eliminar</th>
									
								</tr>
							</thead>
							<tbody >
							</tbody>
						</table>
					</div>
				</div>
		</div>
	</div>

<!-- agregar Modal HTML -->
	<div id="addUserModal" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<form name="add_informacion" id="frm">
					<div class="modal-header">						
						<h4 class="modal-title center">Crear Empleado</h4>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					</div>
					<div class="modal-body">					
						<div class="form-group">
							<div class="row">
								<div class="col-md-4">
									<label>Nombre Completo*</label>
								</div>
								<div class="col-md-8">
									<input type="text" name="nombres"  id="nombres" class="form-control" required>
								</div>
							</div>
							
						</div>
						<div class="form-group">
							<div class="row">
								<div class="col-md-4">
									<label>Correo Electrónico*</label>
								</div>
								<div class="col-md-8">
									<input type="email" name="email"  id="email" class="form-control" required>
								</div>
							</div>
							
						</div>
						<div class="form-group">
							<div class="row">
								<div class="col-md-2"><label>Sexo</label></div>
								<div class="col-md-10">
									<input type="radio" id="opc_1" name="sexo" value="M"> <label for="masculino"> Masculino</label><br>
							 		<input type="radio" id="opc_2" name="sexo" value="F"> <label for="femenino"> Femenino</label><br>
							 		<span></span>
								</div>
							</div>
							 
						</div>
						<div class="form-group">
							<div class="row">
								<div class="col-md-2"><label>Área*</label></div>
								<div class="col-md-10">
									<select style="width: 100%;" id="area" name="area" data-rule-required="true"
                                    data-msg-required="Seleccione un area">
										<option></option>
							 		</select>
								</div>
							</div>
							
							
						</div>
						<div class="form-group">
							<div class="row">
								<div class="col-md-2"><label for="descripcion">Descripción*</label></div>
								<div class="col-md-10">
									<textarea id="descripcion" name="descripcion" rows="4" cols="50"></textarea>

								</div>
							</div>
							
							
						</div>
						<div class="form-group">
							<div class="row">
								<div class="col-md-2"></div>
								<div class="col-md-10">
									<input type="checkbox" id="boletin" name="boletin" value="1"> <label for="boletin"> Deseo recibir boletín informativo</label>

								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="row">
								<div class="col-md-2"><label>Roles*</label></div>
								<div class="col-md-10" id="checkboxinfo">

								</div>
							</div>
						</div>
								
					</div>
					<div class="modal-footer">
						<input type="button" class="btn btn-default" data-dismiss="modal" value="Cancelar">
						<input type="submit" class="btn btn-primary" value="Guardar datos">
					</div>
				</form>
			</div>
		</div>
	</div>
<!-- modal editar -->
<div id="editUserModal" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<form name="edit_informacion" id="frm_edit">
					<div class="modal-header">						
						<h4 class="modal-title center">Editar Empleado</h4>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					</div>
					<div class="modal-body">					
						<div class="form-group">
							<div class="row">
								<div class="col-md-4">
									<label>Nombre Completo*</label>
								</div>
								<div class="col-md-8">
									<input type="text" name="nombres_edit"  id="nombres_edit" class="form-control" required>
									<input type="text" name="id"  id="id" class="form-control" style="display: none;" >
								</div>
							</div>
							
						</div>
						<div class="form-group">
							<div class="row">
								<div class="col-md-4">
									<label>Correo Electrónico*</label>
								</div>
								<div class="col-md-8">
									<input type="email" name="email_edit"  id="email_edit" class="form-control" required>
								</div>
							</div>
							
						</div>
						<div class="form-group">
							<div class="row">
								<div class="col-md-2"><label>Sexo</label></div>
								<div class="col-md-10">
									<input type="radio" id="opc_1_edit" name="sexo_edit" value="M"> <label for="masculino"> Masculino</label><br>
							 		<input type="radio" id="opc_2_edit" name="sexo_edit" value="F"> <label for="femenino"> Femenino</label><br>
							 		<span></span>
								</div>
							</div>
							 
						</div>
						<div class="form-group">
							<div class="row">
								<div class="col-md-2"><label>Área*</label></div>
								<div class="col-md-10">
									<select style="width: 100%;" id="area_edit" name="area_edit" data-rule-required="true"
                                    data-msg-required="Seleccione un area">
										<option></option>
							 		</select>
								</div>
							</div>
							
							
						</div>
						<div class="form-group">
							<div class="row">
								<div class="col-md-2"><label for="descripcion_edit">Descripción*</label></div>
								<div class="col-md-10">
									<textarea id="descripcion_edit" name="descripcion_edit" rows="4" cols="50"></textarea>

								</div>
							</div>
							
							
						</div>
						<div class="form-group">
							<div class="row">
								<div class="col-md-2"></div>
								<div class="col-md-10">
									<input type="checkbox" id="boletin_edit" name="boletin_edit" value="1"> <label for="boletin_edit"> Deseo recibir boletín informativo</label>

								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="row">
								<div class="col-md-2"><label>Roles*</label></div>
								<div class="col-md-10" id="checkboxinfo_edit">

								</div>
							</div>
						</div>
								
					</div>
					<div class="modal-footer">
						<input type="button" class="btn btn-default" data-dismiss="modal" value="Cancelar">
						<input type="submit" class="btn btn-primary" value="Guardar datos">
					</div>
				</form>
			</div>
		</div>
	</div>
<!-- fin -->
<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
<!-- JavaScript Bundle with Popper -->
<script src="css/fontawesome-free/js/fontawesome.js"></script>
<script src="js/validate.js"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js" integrity="sha384-aJ21OjlMXNL5UyIl/XNwTMqvzeRMZH2w8c5cRVpzpU8Y5bApTppSuUkhZXN0VxHd" crossorigin="anonymous"></script>


<script src="js/formulario.js"></script>
</body>
</html>